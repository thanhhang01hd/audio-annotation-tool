let video = document.getElementById('video');
let videoSource = document.getElementById('videoSource');
let customProgress = document.getElementById('customProgress');
let progressBar = document.getElementById('progressBar');
let marker = document.createElement('div');
marker.className = 'marker';
customProgress.appendChild(marker);
let waveformCanvas = document.getElementById('waveformCanvas');
let audioContext = new (window.AudioContext || window.webkitAudioContext)();
let currentActions = [];
let interval;
let annotations = {
    person1: [],
    person2: []
};
let videoFileName = ''; // To store the video file name

const actions = [
    "Humm", 
    "OH", 
    "Wow", 
    "Yeah"
];

function loadVideo(event) {
    let file = event.target.files[0];
    if (file) {
        let url = URL.createObjectURL(file);
        videoSource.src = url;
        video.load();
        extractAudioAndDrawWaveform(url);
        videoFileName = file.name.replace(/\.[^/.]+$/, ""); // Extract and store the video file name without the extension
    }
}

function extractAudioAndDrawWaveform(url) {
    fetch(url)
        .then(response => response.arrayBuffer())
        .then(data => audioContext.decodeAudioData(data))
        .then(buffer => {
            video.audioBuffer = buffer; // Store the audio buffer in the video element for later use
            drawWaveform(buffer);
        });
}

function drawWaveform(buffer) {
    let canvas = waveformCanvas;
    let canvasContext = canvas.getContext('2d');
    canvas.width = window.innerWidth;
    canvas.height = 150;

    let data = buffer.getChannelData(0); // assuming mono channel
    let step = Math.ceil(data.length / canvas.width);
    let amp = canvas.height / 2;

    canvasContext.clearRect(0, 0, canvas.width, canvas.height);
    canvasContext.beginPath();

    for (let i = 0; i < canvas.width; i++) {
        let min = 1.0;
        let max = -1.0;
        for (let j = 0; j < step; j++) {
            let datum = data[(i * step) + j];
            if (datum < min) min = datum;
            if (datum > max) max = datum;
        }
        canvasContext.fillStyle = 'rgba(0, 0, 255, 0.3)'; // Default color
        canvasContext.fillRect(i, (1 + min) * amp, 1, Math.max(1, (max - min) * amp));
    }

    canvasContext.stroke();
}

function updateWaveform() {
    if (!video.audioBuffer) return;

    let canvas = waveformCanvas;
    let canvasContext = canvas.getContext('2d');
    let data = video.audioBuffer.getChannelData(0);
    let step = Math.ceil(data.length / canvas.width);
    let amp = canvas.height / 2;
    let currentTime = video.currentTime;
    let duration = video.duration;

    canvasContext.clearRect(0, 0, canvas.width, canvas.height);
    canvasContext.beginPath();

    for (let i = 0; i < canvas.width; i++) {
        let min = 1.0;
        let max = -1.0;
        for (let j = 0; j < step; j++) {
            let datum = data[(i * step) + j];
            if (datum < min) min = datum;
            if (datum > max) max = datum;
        }
        let percent = i / canvas.width;
        if (percent < (currentTime / duration)) {
            canvasContext.fillStyle = 'rgba(0, 0, 255, 1)'; // Passed segment color
        } else {
            canvasContext.fillStyle = 'rgba(0, 0, 255, 0.3)'; // Default color
        }
        canvasContext.fillRect(i, (1 + min) * amp, 1, Math.max(1, (max - min) * amp));
    }

    canvasContext.stroke();
}

waveformCanvas.addEventListener('click', seekVideoOnWaveform);

function seekVideoOnWaveform(event) {
    const rect = waveformCanvas.getBoundingClientRect();
    const posX = event.clientX - rect.left;
    const percent = posX / rect.width;
    video.currentTime = percent * video.duration;
}

video.addEventListener('timeupdate', () => {
    let percent = (video.currentTime / video.duration) * 100;
    progressBar.style.width = percent + '%';
    marker.style.left = percent + '%';
    updateWaveform(); // Update the waveform dynamically
});

customProgress.addEventListener('click', seekVideo);

function seekVideo(event) {
    const rect = customProgress.getBoundingClientRect();
    const posX = event.clientX - rect.left;
    const percent = posX / rect.width;
    video.currentTime = percent * video.duration;
}

video.addEventListener('play', () => {
    video.addEventListener('timeupdate', updateTable);
});

function startAction(button, person, actionType) {
    let currentAction = currentActions.find(action => action.person === person && action.actionType === actionType);
    if (currentAction) {
        currentAction.endTime = getCurrentTimeInCentiseconds(video.currentTime);
        addAnnotation(currentAction.person, currentAction.actionType, currentAction.startTime, currentAction.endTime);
        currentActions = currentActions.filter(action => !(action.person === person && action.actionType === actionType));
        button.classList.remove('active'); // Reset button color
        button.style.backgroundColor = ''; // Reset to initial color
    } else {
        currentActions.push({
            person: person,
            actionType: actionType,
            startTime: getCurrentTimeInCentiseconds(video.currentTime),
            endTime: null
        });
        button.classList.add('active'); // Change button color to active
        button.style.backgroundColor = 'red'; // Change to red color
    }
}

function initializeActionButtons(person) {
    let buttonGroup = document.querySelector(`#buttons-${person.toLowerCase().replace(' ', '-')} .action-buttons`);
    buttonGroup.innerHTML = ''; // Clear existing buttons
    actions.forEach(actionType => {
        let newButton = document.createElement('button');
        newButton.textContent = actionType;
        newButton.onclick = function() { startAction(this, person, actionType); };
        buttonGroup.appendChild(newButton);
    });
}

function addNewActionButton(person) {
    let actionType = prompt(`Enter the new action type for ${person}:`);
    if (actionType) {
        let buttonGroup = document.querySelector(`#buttons-${person.toLowerCase().replace(' ', '-')} .action-buttons`);
        // Check if the action already exists (case-insensitive)
        let existingButton = Array.from(buttonGroup.children).find(button => button.textContent.toLowerCase() === actionType.toLowerCase());
        if (existingButton) {
            alert(`The action "${actionType}" already exists for ${person}.`);
        } else {
            let newButton = document.createElement('button');
            newButton.textContent = `${actionType}`;
            newButton.onclick = function() { startAction(this, person, actionType); };
            buttonGroup.appendChild(newButton);
        }
    }
}

function addAnnotation(person, actionType, start, end) {
    let tableId = `annotations-table-${person.toLowerCase().replace(' ', '')}`;
    let annotationsTable = document.getElementById(tableId);
    let row = document.createElement('tr');
    row.dataset.start = start;
    row.dataset.end = end;
    row.dataset.person = person;
    let actionCell = document.createElement('td');
    actionCell.textContent = actionType;
    let startCell = document.createElement('td');
    startCell.textContent = formatCentiseconds(start);
    let endCell = document.createElement('td');
    endCell.textContent = formatCentiseconds(end);
    let remarksCell = document.createElement('td');
    let remarksInput = document.createElement('textarea');
    remarksInput.className = 'remarks';
    remarksCell.appendChild(remarksInput);
    let playCell = document.createElement('td');
    let playButton = document.createElement('button');
    playButton.textContent = 'Play';
    playButton.onclick = function() {
        playSegment(start, end);
    };
    playCell.appendChild(playButton);
    let deleteCell = document.createElement('td');
    let deleteButton = document.createElement('button');
    deleteButton.textContent = 'Delete';
    deleteButton.onclick = function() { 
        annotationsTable.removeChild(row);
        removeAnnotation(person, start, end);
    };
    deleteCell.appendChild(deleteButton);
    row.appendChild(actionCell);
    row.appendChild(startCell);
    row.appendChild(endCell);
    row.appendChild(remarksCell);
    row.appendChild(playCell);
    row.appendChild(deleteCell);
    annotationsTable.prepend(row); // Add new row to the top

    // Update annotations array
    annotations[person.toLowerCase().replace(' ', '')].push({ person, actionType, start, end });

    // Render the progress bar immediately
    renderCustomProgress();
}

function playSegment(start, end) {
    clearInterval(interval);
    let startTime = start / 100;
    let endTime = end / 100;
    video.currentTime = startTime;
    video.play();
    interval = setInterval(() => {
        if (video.currentTime >= endTime) {
            video.pause();
            clearInterval(interval);
        }
    }, 10); // Check every 10 milliseconds
}

function saveAllAnnotations() {
    saveAnnotations('Person 1');
    saveAnnotations('Person 2');
    renderCustomProgress();
}

function saveAnnotations(person) {
    let tableId = `annotations-table-${person.toLowerCase().replace(' ', '')}`;
    let annotationsTable = document.getElementById(tableId);
    let data = [];
    for (let row of annotationsTable.rows) {
        let cells = row.cells;
        if (cells.length === 6) {
            data.push({
                action: cells[0].textContent,
                startTime: cells[1].textContent,
                endTime: cells[2].textContent,
                remarks: cells[3].firstChild.value
            });
        }
    }
    let json = JSON.stringify(data);
    let blob = new Blob([json], { type: 'application/json' });
    let url = URL.createObjectURL(blob);
    let a = document.createElement('a');
    a.href = url;
    a.download = `${videoFileName}_${person.toLowerCase().replace(' ', '')}_annotations.json`;
    a.click();
    URL.revokeObjectURL(url);
}

function getCurrentTimeInCentiseconds(timeInSeconds) {
    return Math.floor(timeInSeconds * 100);
}

function formatCentiseconds(centiseconds) {
    let totalSeconds = Math.floor(centiseconds / 100);
    let minutes = Math.floor(totalSeconds / 60);
    let seconds = totalSeconds % 60;
    let cs = centiseconds % 100;
    return `${minutes < 10 ? '0' : ''}${minutes}:${seconds < 10 ? '0' : ''}${seconds}:${cs < 10 ? '0' : ''}${cs}`;
}

function renderCustomProgress() {
    customProgress.innerHTML = '';
    customProgress.appendChild(progressBar);
    customProgress.appendChild(marker);
    let totalDuration = video.duration * 100; // Convert to centiseconds
    let combinedAnnotations = [...annotations.person1, ...annotations.person2];

    combinedAnnotations.forEach(annotation => {
        let segment = document.createElement('div');
        segment.classList.add('timeline-segment');
        segment.style.left = `${(annotation.start / totalDuration) * 100}%`;
        segment.style.width = `${((annotation.end - annotation.start) / totalDuration) * 100}%`;

        let isPerson1 = annotations.person1.some(a => a.start === annotation.start && a.end === annotation.end);
        let isPerson2 = annotations.person2.some(a => a.start === annotation.start && a.end === annotation.end);

        if (isPerson1 && isPerson2) {
            segment.style.backgroundColor = 'red';
        } else if (isPerson1) {
            segment.style.backgroundColor = 'violet';
        } else if (isPerson2) {
            segment.style.backgroundColor = 'blue';
        }

        segment.addEventListener('click', () => highlightTableRows(annotation.start, annotation.end));

        let tooltip = document.createElement('div');
        tooltip.classList.add('tooltip');
        tooltip.textContent = `${annotation.actionType} - ${annotation.person}`;
        segment.appendChild(tooltip);

        customProgress.appendChild(segment);
    });

    document.addEventListener('click', resetHighlights);
}

function highlightTableRows(start, end) {
    resetHighlights();
    const rows = document.querySelectorAll(`tr[data-start='${start}'][data-end='${end}']`);
    rows.forEach(row => row.classList.add('highlight'));
}

function resetHighlights(event) {
    if (!event || !event.target.closest('.timeline-segment')) {
        const highlightedRows = document.querySelectorAll('tr.highlight');
        highlightedRows.forEach(row => row.classList.remove('highlight'));
    }
}

function removeAnnotation(person, start, end) {
    annotations[person.toLowerCase().replace(' ', '')] = annotations[person.toLowerCase().replace(' ', '')].filter(annotation => !(annotation.start === start && annotation.end === end));
    renderCustomProgress();
}

function changePlaybackRate() {
    let playbackRate = document.getElementById('playbackRate').value;
    video.playbackRate = playbackRate;
}

// Initialize buttons for Person 1 and Person 2
initializeActionButtons('Person 1');
initializeActionButtons('Person 2');
